import pandas as pd 
import itertools

#Load PIN data
pin =  pd.read_csv('../data/PIN_genes.txt',sep='\t',names=['GENE']).GENE.tolist()
#Load DGN1 data
DGN1 = pd.read_csv('../data/DGN1_disease_gene.csv',sep='\t',names=['MESH','GENE_1']).drop_duplicates()
print('DGN1:',len(DGN1),'disease-gene associations and',len(DGN1.MESH.unique()),' diseases')

#Check how many diseases in DGN1 have at least 4 genes in PIN (min set to run GUILD)
c = 0
dis_val = []
for disease,dfdis in DGN1.groupby('MESH'):
	genes = list(dfdis.GENE_1.unique())
	genes_pin = list(set(genes).intersection(set(pin)))
	if len(genes_pin) >= 4:
		dis_val.append(disease)
		c+=1
print('DGN1 diseases able to run GUILD:',c)

#Load DGN2 data
DGN2 = pd.read_csv('../data/DGN2_disease_gene.csv',sep='\t',names=['UMLS','GENE_2']).drop_duplicates()
print('DGN2:',len(DGN2),'disease-gene associations and',len(DGN2.UMLS.unique()),'diseases')

#Load DGN1-DGN2 mapping 
ffile = open('../data/MESH_UMLS_mapping.csv','r')
umls_mesh = {}
mesh_umls = {}
for line in ffile.readlines():
	line = line.strip()
	feat = line.split('\t')
	umls_mesh[feat[1]] = feat[0] #do not consider MeSH > 1 UMLS
	mesh_umls[feat[0]] = feat[1]
ffile.close()

def umls_col(row):
	if row['MESH'] in umls_mesh.keys():
		row['UMLS'] = umls_mesh[row['MESH']]
	else:
		row['UMLS'] = row['MESH']
	return row

#Map MESH terms to UMLS
DGN1 = DGN1.apply(umls_col,axis=1)
DGN1 = DGN1[DGN1['MESH'].isin(dis_val)] #filter for diseases in DGN1 with 4 or more seeds 
#print('DGN1 with UMLS to run GUILD:',len(DGN1),'disease-gene associations and,',len(DGN1.MESH.unique()),'diseases')

DGN2_DGN1 = pd.merge(DGN2,DGN1,on='UMLS') #diseases with ../data in DGN2 and DGN2
print('Diseases with data in DGN1 able to run GUILD and in DGN2:',len(DGN2_DGN1.UMLS.unique()))

#Generate file with all diseases to find disease-disease associations
dif_gleft = []
fout = open('../data_generated/DISEASES_to_WORK.txt','w')
for dis,dfdis in DGN2_DGN1.groupby('UMLS'):
	genes_left = set(dfdis.GENE_2.tolist())-set(dfdis.GENE_1.tolist())
	genes_pin = list(set(genes_left).intersection(set(pin)))
	if len(genes_pin) > 0:
		fout.writelines(mesh_umls[dis]+'\n')
		dif_gleft.append(mesh_umls[dis])
fout.close()
print('Diseases with novel gene-disease associations in DGN2 that can run GUILD with DGN1',len(dif_gleft))

#Generate file with all possible disease-pairs from previous set of diseases
fout = open('../data_generated/PAIRS_to_WORK.txt','w')
for pair in list(itertools.combinations(dif_gleft,2)):
	fout.writelines(pair[0]+'.'+pair[1]+'\n')
fout.close()
