from collections import defaultdict
import rpy2.robjects as robjects
from statsmodels.sandbox.stats.multicomp import multipletests
import sys

dicbp = defaultdict(list)
dicbp2 = defaultdict(list)

dicomimtrad = defaultdict(list)
DGN2 = defaultdict(list)
DGN1_e = defaultdict(list)

selected_method = 'GObp'#sys.argv[1]#'GObp'
criteria = 'bonferroni'#sys.argv[2]

file_method = {'GOmf': 'GOmf_DSGPPI.csv', 'GObp': 'GObp_DSGPPI.csv', 'Reactome': 'Reactome_DSGPPI.csv'}

# Loading functional terms
fi3 = open('../data/'+file_method[selected_method])
for line in fi3.readlines():
    gene = line.rstrip().split("	")
    gobp = gene[0]
    geneid = gene[1]

    if gobp not in dicbp[geneid]:
        dicbp[geneid].append(gobp)

    if geneid not in dicbp2[gobp]:
        dicbp2[gobp].append(geneid)

fi3.close()
print(selected_method, 'data loaded')

# Loading DGN1-DGN2 mapping
fi6 = open("../data/MESH_UMLS_mapping.csv")
for line in fi6.readlines():
    gene = line.rstrip().split("	")
    umls = gene[0]
    omimm = gene[1]
    dicomimtrad[umls].append(omimm)
fi6.close()
print('Disease mapping loaded')

# Loading GOterm names
fi6 = open("/home/carlota/projects/comorbidities/alzheimer_case/GOterms_names.txt")
gonames = {}
for line in fi6.readlines():
    f = line.rstrip().split("\t")
    gonames[f[0]] = f[2]
fi6.close()
print('Disease mapping loaded')

# Load DGN2 data and map it to MESH terms
fi7 = open("../data/DGN2_notDGN1_PIN.csv")
for line in fi7.readlines():
    gene = line.rstrip().split("	")
    umls = gene[0]
    geneid = gene[1]

    if umls in dicomimtrad.keys():
        for mesh in dicomimtrad[umls]:
            DGN2[mesh].append(geneid)
fi7.close()
print('DGN2 data loaded')

# Load DGN1 data and GUILD expansion
fi2 = open("../data/DGN1_GUILD_per_disease.csv")
for line in fi2.readlines()[1:]:
    if line.startswith(">>"):
        omim = line.rstrip().replace('>>', '')
    else:
        geneprob = line.rstrip().split()
        gene = geneprob[0]
        DGN1_e[omim].append(gene)

fi2.close()
print('DGN1 data loaded')
DGN1_DGN2_dis = set(DGN1_e.keys()).intersection(set(DGN2.keys()))

allpairs = []
fi = open("../data_generated/PAIRS_to_WORK_Multiple_esclerosis.txt")
for line in fi.readlines():
    allpairs.append(line.strip())
fi.close()

# allpairs = allpairs[sys.argv[1]:sys.argv[2]] #Run for subset

fout = open('../results/' + selected_method + '_FCFraw_' + criteria + '.tsv', 'a')
fout.writelines('Pvalue\tx\tm\tN\tk\n')
# calculating the hypergeometric distribution
foutout = open('/home/carlota/projects/comorbidities/alzheimer_case/ME_GOterms_FCF.tsv','w')
def functional_enrichment(disease, dicbp2, N, geneset_toenrich, criteria):
    pvals = {}
    go_enriched = False
    k = len(geneset_toenrich)
    terms_l = []
    test_passed_f = False
    for term in dicbp2.keys():
        m = len(dicbp2[term])
        xl = [y for y in dicbp2[term] if y in geneset_toenrich]
        x = len(xl)

        if x != 0:
            go_enriched = True
            xlist = []
            for i in range(x, m + 1):
                xlist.append(i)

            # calculation of the hypervalue
            dhyper = robjects.r['dhyper']
            xboh = robjects.IntVector(xlist)
            dhypervalue = dhyper(xboh, m, (N - m), k, log=False)
            # threshold of enrichment
            pvals[term] = sum(dhypervalue)
    if go_enriched:
        pvals_values = list(pvals.values())
        terms = list(pvals.keys())
        pvals_corrected = multipletests(pvals_values, alpha=0.05, method=criteria, is_sorted=False,
                                        returnsorted=False)

        for i in range(0, len(terms)):
            if list(pvals_corrected[1])[i] < 0.05:
                if terms[i] in gonames.keys():
                #     foutout.writelines(disease+'\t'+gonames[terms[i]]+'\t'+str(pvals_values[i])+'\t'+str(list(pvals_corrected[1])[i])+'\n')
                #
                # else:
                    foutout.writelines(disease+'\t'+terms[i]+'\t'+str(pvals_values[i])+'\t'+str(list(pvals_corrected[1])[i])+'\n')

                test_passed_f = True
                terms_l.append(terms[i])
    return test_passed_f, terms_l
for pair in allpairs:

    # print('Calculating....', pair)

    # Disease pair diseases
    gened = pair.split(".")
    disease1 = gened[0]
    disease2 = gened[1]

    if disease1 in DGN1_DGN2_dis and disease2 in DGN1_DGN2_dis:  # If we have data for both diseaes in the pair in DGN1 and DGN2
        # Take common genes
        shared_DGN1_e = list(set(DGN1_e[disease1]).intersection(set(DGN1_e[disease2])))
        shared_DGN2 = list(set(DGN2[disease1]).intersection(set(DGN2[disease2])))

        N = len(dicbp.keys())
        NB = len(dicbp2.keys())

        test_passed_f1, terms_l1 = functional_enrichment(disease1,dicbp2, N, list(set(DGN1_e[disease1])), criteria)
        if test_passed_f1:
            test_passed_f2, terms_l2 = functional_enrichment(disease2, dicbp2, N, list(set(DGN1_e[disease2])), criteria)
            if test_passed_f2:
                shared_DGN1_f = [t1 for t1 in terms_l1 if t1 in terms_l2]
                for g in shared_DGN1_f:
                    print(pair+'\t'+g)
            else:
                continue
        else:
            continue
    else:
        continue
foutout.close()
