import pandas as pd
from statsmodels.sandbox.stats.multicomp import multipletests
import sys

score = sys.argv[1]
criteria = sys.argv[2]

alltests = pd.DataFrame()

##ERROR messages
if score not in ['FCG','FCF','CG']:
    print 'ERROR: not valid score. Choose one of: FCG,FCF,CG'

if criteria not in ['bonferroni','fdr_bh']:
    print 'ERROR: not valid criteria. Choose one of: bonferroni, fdr_bh'


if score in ['FCG','FCF']:
    methods = ['Reactome', 'GObp', 'GOmf']

    for method in methods:
        df = pd.read_csv('../results/' + method +'_'+score+'raw_'+criteria+'.tsv', sep='\t', index_col=False, header=0,
                         names=['PAIR', 'PVAL', 'x', 'm', 'N', 'k']).drop_duplicates()
        pvals_corrected = multipletests(df.PVAL.tolist(), alpha=0.05, method=criteria, is_sorted=False, returnsorted=False)
        df['PVAL_c'] = pvals_corrected[1]
        df['PVAL_c_SIG'] = df['PVAL_c'] < 0.05
        df['METHOD'] = method
        alltests = alltests.append(df)

else:
    df = pd.read_csv('../results/' +score + 'raw.tsv', sep='\t', index_col=False,
                     header=0,
                     names=['PAIR', 'PVAL', 'x', 'm', 'N', 'k']).drop_duplicates()
    pvals_corrected = multipletests(df.PVAL.tolist(), alpha=0.05, method=criteria, is_sorted=False, returnsorted=False)
    df['PVAL_c'] = pvals_corrected[1]
    df['PVAL_c_SIG'] = df['PVAL_c'] < 0.05
    alltests = alltests.append(df)

alltests.to_csv('../results/multipletest_'+score+'_'+criteria+'.tsv', sep='\t', index=False)
