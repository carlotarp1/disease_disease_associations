from collections import defaultdict
import rpy2.robjects as robjects
import sys

dicomimtrad = defaultdict(list)
DGN2 = defaultdict(list)
DGN1_e = defaultdict(list)

criteria = sys.argv[1]#'bonferroni'

# Loading DGN1-DGN2 mapping
fi6 = open("../data/MESH_UMLS_mapping.csv")
for line in fi6.readlines():
    gene = line.rstrip().split("	")
    umls = gene[0]
    omimm = gene[1]
    dicomimtrad[umls].append(omimm)
fi6.close()
print('Disease mapping loaded')

# Load DGN2 data and map it to MESH terms
fi7 = open("../data/DGN2_notDGN1_PIN.csv")
for line in fi7.readlines():
    gene = line.rstrip().split("	")
    umls = gene[0]
    geneid = gene[1]

    if umls in dicomimtrad.keys():
        for mesh in dicomimtrad[umls]:
            DGN2[mesh].append(geneid)
fi7.close()
print('DGN2 data loaded')

# Load DGN1 data and GUILD expansion
fi2 = open("../data/DGN1_GUILD_per_disease.csv")
for line in fi2.readlines()[1:]:
    if line.startswith(">>"):
        omim = line.rstrip().replace('>>', '')
    else:
        geneprob = line.rstrip().split()
        gene = geneprob[0]
        DGN1_e[omim].append(gene)

fi2.close()
print('DGN1 data loaded')
DGN1_DGN2_dis = set(DGN1_e.keys()).intersection(set(DGN2.keys()))
allgenes = []
for g_list in list(DGN1_e.values()):
    for g in g_list:
        allgenes.append(g)
for g_list in list(DGN2.values()):
    for g in g_list:
        allgenes.append(g)
N_allgenes = len(set(allgenes))

allpairs = []
fi = open("../data_generated/PAIRS_to_WORK.txt")
for line in fi.readlines():
    allpairs.append(line.strip())
fi.close()

# allpairs = allpairs[sys.argv[1]:sys.argv[2]] #Run for subset

fout = open('../results/CGraw.tsv', 'a')
fout.writelines('Pvalue\tx\tm\tN\tk\n')
# calculating the hypergeometric distribution
for pair in allpairs:

    print('Calculating....', pair)

    # Disease pair diseases
    gened = pair.split(".")
    disease1 = gened[0]
    disease2 = gened[1]

    if disease1 in DGN1_DGN2_dis and disease2 in DGN1_DGN2_dis:  # If we have data for both diseaes in the pair in DGN1 and DGN2
        # Take common genes
        shared_DGN1_e = list(set(DGN1_e[disease1]).intersection(set(DGN1_e[disease2])))
        shared_DGN2 = list(set(DGN2[disease1]).intersection(set(DGN2[disease2])))

        if len(shared_DGN2) > 0 and len(shared_DGN1_e) > 0:  # If there are common genes
            k = len(shared_DGN1_e)
            m = len(shared_DGN2)

            xtotl = [y for y in shared_DGN1_e if y in shared_DGN2]
            # print xtotl
            xa = len(xtotl)
            if xa != 0:
                print(xa)
                xlist = []
                for i in range(xa, m + 1):
                    xlist.append(i)

                # calculation of the hypervalue
                dhypervalue = 0
                dhyper = robjects.r['dhyper']
                xtot = robjects.IntVector(xlist)
                dhypervalue = dhyper(xtot, m, (N_allgenes - m), k, log=False)
                # threshold of enrichment
                fout.writelines(gened[0] + "." + gened[1] + "\t" + str(sum(dhypervalue)) + '\t' + str(
                    xa) + '\t' + str(m) + '\t' + str((N_allgenes - m)) + '\t' + str(k) + '\n')
                fout.flush()

        else:
            continue
    else:
        continue
