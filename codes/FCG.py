from collections import defaultdict
import rpy2.robjects as robjects
from statsmodels.sandbox.stats.multicomp import multipletests
import sys

dicbp = defaultdict(list)
dicbp2 = defaultdict(list)

dicomimtrad = defaultdict(list)
DGN2 = defaultdict(list)
DGN1_e = defaultdict(list)

selected_method = sys.argv[1]#'Reactome'
criteria = sys.argv[2]#'bonferroni'


file_method = {'GOmf': 'GOmf_DSGPPI.csv', 'GObp': 'GObp_DSGPPI.csv', 'Reactome': 'Reactome_DSGPPI.csv'}

# Loading functional terms
fi3 = open('../data/'+file_method[selected_method])
for line in fi3.readlines():
    gene = line.rstrip().split("	")
    gobp = gene[0]
    geneid = gene[1]

    if gobp not in dicbp[geneid]:
        dicbp[geneid].append(gobp)

    if geneid not in dicbp2[gobp]:
        dicbp2[gobp].append(geneid)

fi3.close()
print(selected_method, 'data loaded')

# Loading DGN1-DGN2 mapping
fi6 = open("../data/MESH_UMLS_mapping.csv")
for line in fi6.readlines():
    gene = line.rstrip().split("	")
    umls = gene[0]
    omimm = gene[1]
    dicomimtrad[umls].append(omimm)
fi6.close()
print('Disease mapping loaded')

# Load DGN2 data and map it to MESH terms
fi7 = open("../data/DGN2_notDGN1_PIN.csv")
for line in fi7.readlines():
    gene = line.rstrip().split("	")
    umls = gene[0]
    geneid = gene[1]

    if umls in dicomimtrad.keys():
        for mesh in dicomimtrad[umls]:
            DGN2[mesh].append(geneid)
fi7.close()
print('DGN2 data loaded')

# Load DGN1 data and GUILD expansion
fi2 = open("../data/DGN1_GUILD_per_disease.csv")
for line in fi2.readlines()[1:]:
    if line.startswith(">>"):
        omim = line.rstrip().replace('>>', '')
    else:
        geneprob = line.rstrip().split()
        gene = geneprob[0]
        DGN1_e[omim].append(gene)

fi2.close()
print('DGN1 data loaded')
DGN1_DGN2_dis = set(DGN1_e.keys()).intersection(set(DGN2.keys()))

allpairs = []
fi = open("../data_generated/PAIRS_to_WORK.txt")
for line in fi.readlines():
    allpairs.append(line.strip())
fi.close()

# allpairs = allpairs[sys.argv[1]:sys.argv[2]] #Run for subset

fout = open('../results/' + selected_method + '_FCGraw_' + criteria + '.tsv', 'a')
fout.writelines('Pvalue\tx\tm\tN\tk\n')
# calculating the hypergeometric distribution
for pair in allpairs:

    print('Calculating....', pair)

    ktot = 0
    mtot = 0
    xtot = 0
    ktotl = []
    mtotl = []
    xtotl = []

    # Disease pair diseases
    gened = pair.split(".")
    disease1 = gened[0]
    disease2 = gened[1]

    if disease1 in DGN1_DGN2_dis and disease2 in DGN1_DGN2_dis:  # If we have data for both diseaes in the pair in DGN1 and DGN2
        # Take common genes
        shared_DGN1_e = list(set(DGN1_e[disease1]).intersection(set(DGN1_e[disease2])))
        shared_DGN2 = list(set(DGN2[disease1]).intersection(set(DGN2[disease2])))

        if len(shared_DGN2) > 0 and len(shared_DGN1_e) > 0:  # If there are common genes

            k = len(shared_DGN1_e)
            k2 = len(shared_DGN2)

            N = len(dicbp.keys())
            NB = len(dicbp2.keys())

            # taking the go terms to see the enrichment for Dv1
            pvals1 = {}
            go_enriched1 = False
            for term in dicbp2.keys():
                m = len(dicbp2[term])
                xl = [y for y in dicbp2[term] if y in shared_DGN1_e]
                x = len(xl)

                if x != 0:
                    go_enriched1 = True
                    xlist = []
                    for i in range(x, m + 1):
                        xlist.append(i)

                    # calculation of the hypervalue
                    dhypervalue = 0
                    dhyper = robjects.r['dhyper']
                    xboh = robjects.IntVector(xlist)
                    dhypervalue = dhyper(xboh, m, (N - m), k, log=False)
                    # threshold of enrichment
                    pvals1[term] = sum(dhypervalue)

                m = 0
                xl = []
                x = 0

            if go_enriched1:
                pvals_values = list(pvals1.values())
                terms = list(pvals1.keys())
                pvals_corrected = multipletests(pvals_values, alpha=0.05, method=criteria, is_sorted=False,
                                                returnsorted=False)

                test_passed_f = False
                for i in range(0, len(terms)):
                    if list(pvals_corrected[1])[i] < 0.05:
                        test_passed_f = True
                        ktotl.append(terms[i])
                        ktot += 1

                if test_passed_f:
                    pvals2 = {}
                    go_enriched2 = False
                    # taking the go terms to see the enrichment for Dv2
                    for term in dicbp2.keys():
                        m = len(dicbp2[term])
                        xl = [y for y in dicbp2[term] if y in shared_DGN2]
                        x = len(xl)

                        if x != 0:
                            go_enriched2 = True
                            xlist = []
                            for i in range(x, m + 1):
                                xlist.append(i)

                            # calculation of the hypervalue
                            dhypervalue = 0
                            dhyper = robjects.r['dhyper']
                            xboh = robjects.IntVector(xlist)
                            dhypervalue = dhyper(xboh, m, (N - m), k2, log=False)
                            pvals2[term] = sum(dhypervalue)

                        m = 0
                        xl = []
                        x = 0

                    if go_enriched2:
                        pvals_values2 = list(pvals2.values())
                        terms2 = list(pvals2.keys())
                        pvals_corrected2 = multipletests(pvals_values2, alpha=0.05, method=criteria,
                                                         is_sorted=False, returnsorted=False)
                        test2_passed_f = False
                        for i in range(0, len(terms2)):
                            if list(pvals_corrected2[1])[i] < 0.05:
                                test2_passed_f = True
                                mtotl.append(terms2[i])
                                mtot += 1

                        if test2_passed_f:
                            # final enrichment
                            xtotl = [y for y in ktotl if y in mtotl]
                            # print xtotl
                            xa = len(xtotl)
                            if xa != 0:
                                xlist = []
                                for i in range(xa, mtot + 1):
                                    xlist.append(i)

                                # calculation of the hypervalue
                                dhypervalue = 0
                                dhyper = robjects.r['dhyper']
                                xtot = robjects.IntVector(xlist)
                                dhypervalue = dhyper(xtot, mtot, (NB - mtot), ktot, log=False)
                                # threshold of enrichment
                                fout.writelines(gened[0] + "." + gened[1] + "\t" + str(sum(dhypervalue)) + '\t' + str(
                                    xa) + '\t' + str(mtot) + '\t' + str((NB - mtot)) + '\t' + str(ktot) + '\n')
                                fout.flush()
                            else:
                                continue
                        else:
                            continue
                    else:
                        continue
                else:
                    continue
            else:
                continue
        else:
            continue
    else:
        continue
