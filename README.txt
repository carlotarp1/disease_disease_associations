#### REQUIREMENTS
Python 3
rpy2
numpy
pandas


##Code for generation of the list of diseases and disease pairs to run the 7 disease relationship scores
####output: PAIRS_to_WORK.txt DISEASES_to_WORK.txt
initial_disease_selection.py

##Code for running FCG scores either for GObp,GOmf or Reactome and criteria bonferroni or fdr_bh.
##Run it through command line first argument the method (method=GObp,GOmf,Reactome) second argument criteria (criteria = bonferroni,fdr_bh).
###output: method_FCG.tsv
FCG.py method criteria

##Code for running FCF scores either for GObp,GOmf or Reactome and criteria bonferroni or fdr_bh.
##Run it through command line first argument the method (method=GObp,GOmf,Reactome) second argument criteria (criteria = bonferroni,fdr_bh).
###output: method_FCG.tsv
FCF.py method criteria

##Code for running a multiple-testing correction on all scores (FCG, FCF, CG) and methods, for FCF and FCG (GObp, GOmf, Reactome).
##Run it through command line first argument the score (score=FCF,FCG,CG) second argument criteria (criteria = bonferroni,fdr_bh).
final_multipletest.py score criteria

##Additional analysis generated after running previous codes (i.e. generation of paper supplementary tables)
aux_analysis/