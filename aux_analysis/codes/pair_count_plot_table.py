import matplotlib.pylab as plt
import pandas as pd
from matplotlib_venn import venn3


#Generate Table S2

#Load mesh names
mesh_name = {}
fi2 = open("../data/MESH_completNAMES.txt",'r')
for line in fi2.readlines():
    dis = line.strip().split('\t')
    mesh_name[dis[0]] = dis[1]
fi2.close()


scores = ['FCF','FCG','CG']

allscores = pd.DataFrame()
for m in scores:
    df = pd.read_csv('../../results/multipletest_'+m+'_bonferroni.tsv',sep='\t',header=0,index_col=False)
    df = df[df['PVAL_c_SIG']==True]
    if 'METHOD' not in df.columns.tolist():
        df['METHOD'] = ''
    df['SCOREfull'] = m +'-'+ df['METHOD']
    df['SCORE'] = m
    allscores = allscores.append(df)

scores_names = sorted(allscores.SCOREfull.unique().tolist())
fout = open('../results/Table_S2A.csv','w')
fout.writelines('Disease 1 ID\tDisease 1 name\tDisease 2 ID\tDisease 2 name\t'+'\t'.join(scores_names)+'\tComposite_score\n')
for pair, dfpair in allscores.groupby('PAIR'):
    dis1 = pair.split('.')[0]
    dis2 = pair.split('.')[1]
    dis1_name = mesh_name[dis1]
    dis2_name = mesh_name[dis2]
    pvals = []
    comp_score = 0
    for score in scores_names:
        df_score = dfpair[dfpair['SCOREfull']==score]

        if len(df_score) > 0:
            pval = str(df_score.PVAL_c.tolist()[0])
            comp_score += 1
        else:
            pval = 'NS'

        pvals.append(pval)

    fout.writelines(dis1+'\t'+dis1_name+'\t'+dis2+'\t'+dis2_name+'\t'+'\t'.join(pvals)+'\t'+str(comp_score)+'\n')
fout.close()
print('Table S2A generated')

allscores_fdr = pd.DataFrame()
for m in scores:
    df = pd.read_csv('../../results/multipletest_'+m+'_fdr_bh.tsv',sep='\t',header=0,index_col=False)
    df = df[df['PVAL_c_SIG']==True]
    if 'METHOD' not in df.columns.tolist():
        df['METHOD'] = ''
    df['SCOREfull'] = m +'-'+ df['METHOD']
    df['SCORE'] = m
    allscores_fdr = allscores_fdr.append(df)

scores_names = sorted(allscores_fdr.SCOREfull.unique().tolist())
fout = open('../results/Table_S2B.csv','w')
fout.writelines('Disease 1 ID\tDisease 1 name\tDisease 2 ID\tDisease 2 name\t'+'\t'.join(scores_names)+'\tComposite_score\n')
for pair, dfpair in allscores_fdr.groupby('PAIR'):
    dis1 = pair.split('.')[0]
    dis2 = pair.split('.')[1]
    dis1_name = mesh_name[dis1]
    dis2_name = mesh_name[dis2]
    pvals = []
    comp_score = 0
    for score in scores_names:
        df_score = dfpair[dfpair['SCOREfull']==score]

        if len(df_score) > 0:
            pval = str(df_score.PVAL_c.tolist()[0])
            comp_score +=1
        else:
            pval = 'NS'

        pvals.append(pval)

    fout.writelines(dis1+'\t'+dis1_name+'\t'+dis2+'\t'+dis2_name+'\t'+'\t'.join(pvals)+'\t'+str(comp_score)+'\n')
fout.close()
print('Table S2B generated')

#Figure 2A
pairst = []
dist = []
i = 1
xlabel = []
ally1 = []
ally2 = []
for score, scoredf in allscores_fdr.groupby('SCOREfull'):
    dis = [p.split('.')[0] for p in scoredf.PAIR.tolist()]
    dis.extend([p.split('.')[1] for p in scoredf.PAIR.tolist()])
    pairst += scoredf.PAIR.tolist()
    dist += dis

    plt.bar(i, len(scoredf.PAIR.tolist()), 0.35, color='y')
    plt.bar(i + 0.35, len(set(dis)), 0.35, color='r')
    print(score, len(set(scoredf.PAIR.tolist())), len(set(dis)))
    xlabel.append(score)
    i += 1

plt.bar(i, len(set(pairst)), 0.35, color='y')
plt.bar(i + 0.35, len(set(dist)), 0.35, color='r')
print('Total',len(set(pairst)),len(set(dist)))
xlabel.append('Total')

plt.xticks([1,2,3,4,5,6,7,8],xlabel,rotation=90)
plt.savefig('../results/figs3_prelim_a.svg')
print('Fig 2A generated')




#Figure 2B
set1 = set(allscores_fdr[allscores_fdr['SCORE']=='CG'].PAIR.tolist())
set2 = set(allscores_fdr[allscores_fdr['SCORE']=='FCG'].PAIR.tolist())
set3 = set(allscores_fdr[allscores_fdr['SCORE']=='FCF'].PAIR.tolist())
venn3([set1, set2, set3], ('CG', 'FCG', 'FCF'))
plt.savefig('../results/figs3_prelim_b.svg')
print('Fig 2B generated')
