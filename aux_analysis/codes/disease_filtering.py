import pandas as pd
import itertools

# Load PIN data
pin = pd.read_csv('../../data/PIN_genes.txt', sep='\t', names=['GENE']).GENE.tolist()
# Load DGN1 data
DGN1 = pd.read_csv('../../data/DGN1_disease_gene.csv', sep='\t', names=['MESH', 'GENE_1']).drop_duplicates()
print 'DGN1:', len(DGN1), 'disease-gene associations and', len(DGN1.MESH.unique()), ' diseases'

# Check how many diseases in DGN1 have at least 4 genes in PIN (min set to run GUILD)
c = 0
dis_val = []
for disease, dfdis in DGN1.groupby('MESH'):
    genes = list(dfdis.GENE_1.unique())
    genes_pin = list(set(genes).intersection(set(pin)))
    if len(genes_pin) >= 4:
        dis_val.append(disease)
        c += 1

print 'DGN1 diseases to run GUILD', c

# Load DGN2 data
DGN2 = pd.read_csv('../../data/DGN2_disease_gene.csv', sep='\t', names=['UMLS', 'GENE_2']).drop_duplicates()
print 'DGN2:', len(DGN2), 'disease-gene associations and', len(DGN2.UMLS.unique()), 'diseases'

# Load DGN1-DGN2 mapping
ffile = open('../../data/MESH_UMLS_mapping.csv', 'r')
umls_mesh = {}
mesh_umls = {}
for line in ffile.readlines():
    line = line.strip()
    feat = line.split('\t')
    umls_mesh[feat[1]] = feat[0]  # do not consider MeSH > 1 UMLS
    mesh_umls[feat[0]] = feat[1]
ffile.close()


def umls_col(row):
    if row['MESH'] in umls_mesh.keys():
        row['UMLS'] = umls_mesh[row['MESH']]
    else:
        row['UMLS'] = row['MESH']
    return row


# Map MESH terms to UMLS
DGN1 = DGN1.apply(umls_col, axis=1)
DGN1 = DGN1[DGN1['MESH'].isin(dis_val)]  # filter for diseases in DGN1 with 4 or more seeds

DGN2_DGN1 = pd.merge(DGN2, DGN1, on='UMLS')  # diseases with data in DGN2 and DGN2
print 'Diseases with data in DGN1 to run GUILD and in DGN2:', len(DGN2_DGN1.UMLS.unique())

## Generate file with all valid diseases to find disease-disease associations
dif_gleft = []
# fout = open('../../data_generated/DISEASES_to_WORK.txt', 'w')
for dis, dfdis in DGN2_DGN1.groupby('UMLS'):
    genes_left = set(dfdis.GENE_2.tolist()) - set(dfdis.GENE_1.tolist())
    genes_pin = list(set(genes_left).intersection(set(pin)))
    if len(genes_pin) > 0:
        # fout.writelines(mesh_umls[dis] + '\n')
        if mesh_umls[dis] in dis_val:
            dif_gleft.append(mesh_umls[dis])
# fout.close()

print 'Diseases with novel gene-disease associations in DGN2 that can run GUILD', len(set(dif_gleft))
# Generate file with all diseases to run the methods
fout = open('../../data_generated/DISEASES_to_WORK.txt', 'w')
for disease in set(dif_gleft):
    fout.writelines(disease + '\n')
fout.close()


# Generate file with all possible disease-pairs from previous set of diseases
fout = open('../../data_generated/PAIRS_to_WORK.txt', 'w')
for pair in list(itertools.combinations(dif_gleft, 2)):
    fout.writelines(pair[0] + '.' + pair[1] + '\n')
fout.close()

## Generate Table S1

# Load DGN1 data and GUILD expansion
DGN1_e = {}
fi2 = open("../../data/DGN1_GUILD_per_disease.csv")
for line in fi2.readlines()[1:]:
    if line.startswith(">>"):
        mesh = line.rstrip().replace('>>', '')
        DGN1_e[mesh] = []
    else:
        geneprob = line.rstrip().split()
        gene = geneprob[0]
        DGN1_e[mesh].append(gene)
fi2.close()

# Load MESH names
mesh_name = {}
fi2 = open("../data/MESH_completNAMES.txt",'r')
for line in fi2.readlines():
    dis = line.strip().split('\t')
    mesh_name[dis[0]] = dis[1]
fi2.close()

fout = open('../results/Table_S1.csv','w')
for dis, genes in DGN1_e.iteritems():
    if dis in dif_gleft:
        DGN1_genes = set(DGN1[DGN1['MESH']==dis].GENE_1.tolist())
        DGN2_genes = set(DGN2_DGN1[DGN2_DGN1['MESH'] == dis].GENE_2.tolist())
        genes = set(genes)
        fout.writelines(dis+'\t'+mesh_name[dis]+'\t'+str(len(DGN1_genes))+'\t'+str(len(genes))+'\t'+str(len(DGN2_genes))+'\n')
fout.close()