import pandas as pd
import matplotlib.pylab as plt

DGN1_e = {}
fi2 = open("../data/DGN1_GUILD_per_disease.csv")
for line in fi2.readlines()[1:]:
    if line.startswith(">>"):
        omim = str(line.rstrip().replace('>>', ''))
        DGN1_e[omim] = []
    else:
        geneprob = line.rstrip().split()
        gene = geneprob[0]
        if str(geneprob[1]) == '100':
            DGN1_e[omim].append(gene)
fi2.close()

allpairs = []
fi = open("../data_generated/PAIRS_to_WORK.txt")
for line in fi.readlines():
    allpairs.append(line.strip())
fi.close()

pairs_jacc = []
for pair in allpairs:
    disease1 = pair.split('.')[0]
    disease2 = pair.split('.')[1]

    if disease1 in DGN1_e.keys() and disease2 in DGN1_e.keys():
        shared_DGN1_e = list(set(DGN1_e[disease1]).intersection(set(DGN1_e[disease2])))

        if len(shared_DGN1_e) > 0:
            jaccard = len(shared_DGN1_e) / float(len(set(DGN1_e[disease1]))+len(set(DGN1_e[disease2]))-len(shared_DGN1_e))
            pairs_jacc.append({'PAIR':pair,'JACCARD':jaccard})

pairs_jacc_df = pd.DataFrame(pairs_jacc)
print(pairs_jacc_df[pairs_jacc_df['JACCARD']>=0.5])
# jaccs = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
# total_samps = len(pairs_jacc_df)
# y = []
# x = []
# for i in range(0,len(jaccs)):
#     if i != 0:
#         pairs_r = pairs_jacc_df[(pairs_jacc_df['JACCARD'] <= jaccs[i])&(pairs_jacc_df['JACCARD'] > jaccs[i-1])]
#     else:
#         pairs_r = pairs_jacc_df[pairs_jacc_df['JACCARD']==jaccs[i]]
#
#     x.append(jaccs[i])
#     y.append(len(pairs_r)/float(total_samps))
#
#     print(jaccs[i],len(pairs_r))
#
# plt.bar(x,y,width=0.07,color='#444444')
# plt.xticks(jaccs)
# plt.savefig('../results/pairs_jaccard.svg')